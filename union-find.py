import array
import time

def get_time_stamp():
    t = time.time()
    return int(round(t * 1000))
def quick_find(filename):
    print("please input i:")
    i = int(input())
    ids = []
    N=i**10
    for v in range(N):
        ids.append(v)

    with open(filename, 'r') as f:
        while True:
            line = f.readline()
            if not line:
                return
            line = line.split()
            numbers = []
            for i in line:
                numbers.append(int(i))
            if len(numbers) != 2:
                return
            if ids[numbers[0]] == ids[numbers[1]]:
                continue
            t=ids[numbers[0]]
            stime=get_time_stamp()
            for j in range(N):
                if ids[j]==t:
                    ids[j]=ids[numbers[1]]
            etime=get_time_stamp()
            print('Duration:%d' % (etime-stime))
            print(numbers[0], numbers[1])
    return
def quick_union(filename):
    print("please input i:")
    i = int(input())
    ids = []
    N = i ** 10
    for v in range(N):
        ids.append(v)
    with open(filename, 'r') as f:
        while True:
            line = f.readline()
            if not line:
                return
            line = line.split()
            numbers = []
            for i in line:
                numbers.append(int(i))
            if len(numbers) != 2:
                return
            stime = get_time_stamp()
            p=numbers[0]
            q=numbers[1]
            i=p
            while(1):
                if i==ids[i]:
                    break
                i=ids[i]
            j=q
            while(1):
                if j==ids[j]:
                    break
                j=ids[j]
            if i==j:
                continue
            ids[i]=j
            etime = get_time_stamp()
            print('Duration:%d' % (etime - stime))
            print(p,q)
    return
def weighted_QU(filename):
    print("please input i:")
    i = int(input())
    ids = []
    sz=[]
    N = i ** 10
    for v in range(N):
        ids.append(v)
        sz.append(1)
    with open(filename, 'r') as f:
        while True:
            line = f.readline()
            if not line:
                return
            line = line.split()
            numbers = []
            for i in line:
                numbers.append(int(i))
            if len(numbers) != 2:
                return
            stime = get_time_stamp()
            p = numbers[0]
            q = numbers[1]
            i = p
            while (1):
                if i == ids[i]:
                    break
                i = ids[i]
            j = q
            while (1):
                if j == ids[j]:
                    break
                j = ids[j]
            if i == j:
                continue
            if sz[i] < sz[j]:
                ids[i]=j
                sz[j]=sz[j]+sz[i]
            else:
                ids[j]=i
                sz[i]=sz[i]+sz[j]
            etime = get_time_stamp()
            print('Duration:%d' % (etime - stime))
            print(p, q)
    return
def path_compress(filename):
    print("please input i:")
    i = int(input())
    ids = []
    sz = []
    N = i ** 10
    for v in range(N):
        ids.append(v)
        sz.append(1)
    with open(filename, 'r') as f:
        while True:
            line = f.readline()
            if not line:
                return
            line = line.split()
            numbers = []
            for i in line:
                numbers.append(int(i))
            if len(numbers) != 2:
                return
            stime = get_time_stamp()
            p = numbers[0]
            q = numbers[1]
            i = p
            while (1):
                if i == ids[i]:
                    break
                ids[i]=ids[ids[i]]
                i = ids[i]
            j = q
            while (1):
                if j == ids[j]:
                    break
                ids[i]=ids[ids[j]]
                j = ids[j]
            if i == j:
                continue
            if sz[i] < sz[j]:
                ids[i] = j
                sz[j] = sz[j] + sz[i]
            else:
                ids[j] = i
                sz[i] = sz[i] + sz[j]
            etime = get_time_stamp()
            print('Duration:%d' % (etime - stime))
            print(p, q)
    return
quick_find("input.txt")
quick_union("input.txt")
weighted_QU("input.txt")
path_compress("input.txt")